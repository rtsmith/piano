import { Subject } from 'rxjs';

// singleton stream of note data
export class NoteStream {
  constructor() {
    this._stream = new Subject()
  }

  get stream() {
    return this._stream
  }
}
