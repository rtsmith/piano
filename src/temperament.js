// See e.g. http://www.phy.mtu.edu/~suits/NoteFreqCalcs.html

const base = 440; // A
const numSteps = 12; // 12
const getStepSize = function () {
  return Math.pow(2, 1 / numSteps);
};

export function noteFrequency(step) {
  // Return hz of the step, e.g. for the default settings tihs yields:
  // 0 = 440 (A)
  // 5 = 587.329... (D)
  // -12 = 220 
  return Math.pow(getStepSize(), step) * base;
}
