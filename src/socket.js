export class Socket {
  constructor(noteBuss) {
    this.socket = new WebSocket('ws://localhost:3000/cable')
    
    // subscribe to setup
    this.socket.addEventListener('open', (e) => {
      const subscription = JSON.stringify({
        command: 'subscribe',
        identifier: JSON.stringify({
          channel: 'ApplicationCable::SetupChannel'
        })
      })
      this.socket.send(subscription)
    })
    // subscribe to notes
    this.socket.addEventListener('open', (e) => {
      const subscription = JSON.stringify({
        command: 'subscribe',
        identifier: JSON.stringify({
          channel: 'ApplicationCable::NotesChannel'
        })
      })
      this.socket.send(subscription)
    })


    // listen for responses
    this.socket.addEventListener('message', (e) => {
      const data = JSON.parse(e.data)

      console.log("Message from server: ", JSON.parse(e.data))
      // if (data.identifier)

      if (data.message && data.message.user)
        this.uuid = data.message.user
        console.log(this.uuid)

      if (data.message && data.message.note) {
        if (data.message.user === this.uuid) {
          return;
        }
        // apply key presses from server
        noteBuss.next(data.message.note.note)
      }
    })

    // send keys pressed on this client to server
    noteBuss.subscribe(note => this.sendNote(note))
  }

  sendNote(note) {
    delete note.origin
    const message = JSON.stringify({
      command: 'message',
      identifier: JSON.stringify({
        channel: 'ApplicationCable::NotesChannel'
      }),
      data: JSON.stringify({
        action: 'receive_note',
        note: note
      })
    })
    this.socket.send(message)
  }
}
