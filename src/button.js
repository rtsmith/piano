import { fromEvent } from 'rxjs';
import { scan } from 'rxjs/operators'

export class Button {
  constructor(note, stream) {
    this.button = document.createElement('button')
    this.buttonText = note

    fromEvent(this.button, 'mousedown').subscribe(() => {
      stream.next({note: note, on: true, local: true})
    })

    fromEvent(this.button, 'mouseup').subscribe(() => {
      stream.next({note: note, on: false, local: true})
    })

  }

  set buttonText(text) {
    this.button.textContent = ""
    this.button.appendChild(document.createTextNode(text))
  }

  get el() {
    return this.button
  }
}
