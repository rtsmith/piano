/* eslint-env browser */
import './assets/css/style.css';
import { Button } from './button.js';
import { NoteStream } from './noteStream.js';
import { Synth } from './synth.js';
import { Socket } from './socket.js';

const _keys = [...Array(24).keys()]
let keys = _keys.map((k) => {
  return {
    n: k + 24,
  }
})

const noteBuss = new NoteStream()
const synth = new Synth(noteBuss.stream)
// const socket = new Socket(noteBuss.stream)

const title = document.createElement('h1');
title.textContent = 'OI OI OI!!!!';
title.className = 'title';

const buttons = keys.map((k) => {
  return new Button(k.n, noteBuss.stream)
})

const app = document.getElementById('app');

if (app) {
  app.appendChild(title);
  buttons.forEach((b) => {
    app.appendChild(b.el)
  })
}
