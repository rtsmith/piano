import { noteFrequency } from './temperament.js';

export class Synth {
  constructor(buss) {
    this.noteBuss = buss
    this.audioCtx = new (window.AudioContext || window.webkitAudioContext)()
    this.osc = this.audioCtx.createOscillator()
    this.gain = this.audioCtx.createGain()
    this.gain.gain.value = 0
    this.osc.connect(this.gain)
    this.gain.connect(this.audioCtx.destination)

    this.osc.type = "square"
    this.osc.start()

    this.noteBuss.subscribe((note) => {
      console.log(note)
      note.on ? this.startNote(note) : this.stopNote(note)
    })
  }

  startNote(note) {
    this.osc.frequency.value = noteFrequency(note.note)
    this.gain.gain.value = 1.0
  }

  stopNote(note) {
    this.gain.gain.value = 0.0
  }
}
